---
title: Επιστημονικοί υπολογισμοί υψηλών επιδόσεων με την γλώσσα προγραμματισμού Julia
author: Θεόδωρος Γεροστάθης
---

# Επιστημονικοί υπολογισμοί υψηλών επιδόσεων με την γλώσσα προγραμματισμού Julia

## Γνωριμία με τη γλώσσα προγραμματισμού Julia

  ### Μέρος Ι

  - [Εγκατάσταση](./01_part/00_Installation.ipynb)
  - [Γιατί Julia;](./01_part/01_why_Julia.ipynb)
  - [Βασικοί τύποι δεδομένων](./01_part/02_data_types.ipynb)
  - [Δομές ελέγχου και επαναλήψεων](./02_part/03_conditionals_and_loops.ipynb)
  - [Συναρτήσεις και μέθοδοι](./02_part/04_functions.ipynb)
  - Συλλογές
    - [Λίστες τιμών, πλειάδες, σύνολα, λεξικά (ranges, tuples, sets, dictionaries)](./02_part/05_collections.ipynb)
    - [Είδη πινάκων και Γραμμική Άλγεβρα](./03_part/06_arrays.ipynb)
  - [Οπτικοποίηση δεδομένων (data visualization, plotting)](/03_part/plots_example/plots_example.ipynb)
  - [Μέτρηση επιδόσεων](./01_part/01_why_Julia.ipynb)
  
  ### Μέρος ΙΙ

  - [Σύνθετες δομές δεδομένων (structs)](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/04_part)
  - [Βιβλιοθήκες (modules)](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/07_part)
    - [Δημιουργία Modules](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/07_part)
    - [Έλεγχος κώδικα (testing)](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/07_part)
  - [Στοιχεία συναρτησιακού προγραμματισμού (functional programming)](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/05_part)
  - [Συνθεσιμότητα (composability)](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/05_part)
  - [Μεταπρογραμματισμός (metaprogramming)](https://gitlab.com/tgero/HPC-with-Julia/-/blob/main/06_part/metaprogramming.ipynb)
  - [Συμβολικοί υπολογισμοί](https://gitlab.com/tgero/HPC-with-Julia/-/blob/main/06_part/symbolic_programming.ipynb)

## [Πρακτικές για αποδοτικούς υπολογισμούς](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/08_part)
  - Διαχείριση μνήμης (Stack και Heap)
  - Διανυσματοποίηση (vectorization)
  - Δημιουργία (μικρών) arrays στο stack

## [Πολυνηματικοί υπολογισμοί (Multithreading)](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/08_part)
  - Πολυνηματικές δομές επαναλήψεων
  - Εργασίες (tasks)
  - Χρήσιμα πακέτα για πολυνηματικούς υπολογισμούς

## Πολυεπεξεργασία (Multiprocessing)
  - [Εντολές πολυεπεξεργασίας στη Julia](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/09_part)
  - [Πολυεπεξεργασία σε συστοιχία](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/10_part)
  - [Message Passing (MPI)](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/10_part)
  
## [Εισαγωγή στον προγραμματισμό σε κάρτες γραφικών (GPU Programming)](https://gitlab.com/tgero/HPC-with-Julia/-/tree/main/11_part)
  - Προγραμματισμός σε υψηλό επίπεδο
  - Πυρήνες CUDΑ
  - Βιβλιοθήκες CUDA

## Σύνδεσμοι

### Online μαθήματα
- [Julia Academy](https://juliaacademy.com/)
- [Introduction to Computational Thinking](https://computationalthinking.mit.edu/)
- [Introduction to Scientific Programming and Machine Learning with Julia](https://sylvaticus.github.io/SPMLJ/stable/)

### Cheatsheets
- [MATLAB–Python–Julia Cheatsheet](https://cheatsheets.quantecon.org/)
- [Julia By Example](https://juliabyexample.helpmanual.io/)
- [Plot.jl Cheatsheet](https://github.com/sswatson/cheatsheets/blob/master/plotsjl-cheatsheet.pdf)

### Βιβλία
- Lauwens, B. (2018). Think Julia: How to Think Like a Computer Scientist. <https://benlauwens.github.io/ThinkJulia.jl/latest/book.html>
- Kaminski, B. (2022). The Julia Express. Accessed 18 March 2023. <http://bogumilkaminski.pl/files/julia_express.pdf>
- Kalicharan, N. (2021). Julia - Bit by Bit: Programming for Beginners. Springer International Publishing. <https://doi.org/10.1007/978-3-030-73936-2>
- Lobianco, A. (2019). Julia Quick Syntax Reference: A Pocket Guide for Data Science Programming. Apress. <https://doi.org/10.1007/978-1-4842-5190-4>
- Heitzinger, C. (2022). Algorithms with JULIA: Optimization, Machine Learning, and Differential Equations Using the JULIA Language. Springer International Publishing. <https://doi.org/10.1007/978-3-031-16560-3>
- Boyd, S. P., & Vandenberghe, L. (2018). Introduction to applied linear algebra: Vectors, matrices, and least squares. Cambridge University Press.
<https://web.stanford.edu/~boyd/vmls/>
- Nazarathy, Y., & Klok, H. (2021). Statistics with Julia: Fundamentals for Data Science, Machine Learning and Artificial Intelligence. Springer International Publishing. <https://doi.org/10.1007/978-3-030-70901-3>
- Chan, S. H. (2021). Introduction to Probability for Data Science
An undergraduate textbook on probability for data science. Michigan Publishing. <https://probability4datascience.com/index.html>
- Kochenderfer, M. J., Wheeler, T. A., & Wray, K. H. (2022). Algorithms for decision making. The MIT Press. <https://algorithmsbook.com/>
- Storopoli, Huijzer and Alonso (2021). Julia Data Science. <https://juliadatascience.io>. ISBN: 9798489859165

### Άρθρα
- Bezanson, J., Edelman, A., Karpinski, S., & Shah, V. B. (2017). Julia: A Fresh Approach to Numerical Computing. SIAM Review, 59(1), 65–98. <https://doi.org/10.1137/141000671>, <https://julialang.org/blog/2017/03/julia-fresh-paper/>
- Rackauckas, C. (n.d.). A Comparison Between Differential Equation Solver Suites In MATLAB, R, Julia, Python, C, Mathematica, Maple, and Fortran [Data set]. <https://doi.org/10.15200/winn.153459.98975>
