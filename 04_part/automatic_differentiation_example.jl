#=

Γεροστάθης Θεόδωρος

Παράδειγμα χρήσης του module ForwardDiff για την
παραγώγιση συναρτήσεων μια μεταβλητής

=#

using Plots
using LaTeXStrings
using ForwardDiff

df_analytic(x) = 3x^2-4x + 5

f(x) = x^3-2x^2+5x+1
df(x) = ForwardDiff.derivative(f, x)

x=LinRange(-10, 10, 200)

fig1 = plot(x, df_analytic.(x), linestyle=:solid, color=:black,
     label = "analytic",
     framestyle = :box,
     grid = (:all, :blue, :dot, 1, 0.3))
plot!(x, df.(x), linestyle=:dash, alpha=0.7,
     label = "using ForwardDiff", color = :maroon1,
     linewidth=5)
xlabel!("x")
ylabel!(L"\frac{df}{dx}")

display(fig1)

x = LinRange(0,π, 401)
f4(x) = cos(exp(x))

df4_analytic(x) = -sin(exp(x))*exp(x)
df4(x) = ForwardDiff.derivative(f4, x)


fig4 = plot(x, f4.(x), linestyle=:solid, color=:blue,
            label = L"f(x)",
            framestyle = :box,
            grid = (:all, :blue, :dot, 1, 0.3))
plot!(x, df4_analytic.(x), linestyle=:solid, color=:green,
     label = L"analytic \frac{df}{dx}")
plot!(x, df4.(x), linestyle=:dash, alpha=0.5,
     label = L"ForwardDiff \frac{df}{dx}", color = :maroon1,
     linewidth=5)
xlabel!("x")
ylabel!(L"\frac{df}{dx}")
display(fig4)
