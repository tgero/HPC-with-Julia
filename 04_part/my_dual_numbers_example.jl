#=

Γεροστάθης Θεόδωρος

Παράδειγμα δημιουργίας custom type για την αυτόματη παραγώγιση συναρτήσεων 
με εφαρμογή των dual numbers.

το παράδειγμα είναι βασισμένο στο άρθρο του Erik Engheim στη σελίδα:

<https://nextjournal.com/erik-engheim/implementation-of-automatic-differentiation>

Επίσης δείτε τα:

https://www.redalyc.org/journal/467/46761359006/html/

https://en.wikipedia.org/wiki/Dual_number

https://en.wikipedia.org/wiki/Automatic_differentiation#Automatic_differentiation_using_dual_numbers


=#

using Plots
using LaTeXStrings


import Base: show, +, -, *, /
import Base: exp, log, abs
import Base: sin, cos, tan
import Base: cosh

import Base: convert, promote_rule

struct MyDual{T<:Real} <: Real
    a::T
    b::T
end


# Constructor με input αριθμό <: Real

MyDual(x) = MyDual(x, one(x)) 

show(io::IO, x::MyDual) = print(io, x.a, " + ", x.b, "ϵ")

@inline +(x::MyDual, y::MyDual) = MyDual(x.a+y.a, x.b+y.b)
@inline -(x::MyDual, y::MyDual) = MyDual(x.a-y.a, x.b-y.b)

# Γινόμενο
@inline *(x::MyDual, y::MyDual) = MyDual(x.a*y.a, y.a*x.b + x.a*y.b)

# Πηλίκο

@inline /(x::MyDual, y::MyDual) = MyDual(x.a / y.a, (x.b*y.a - y.b*x.a) / y.a^2)


# Παράδειγμα αριθμητικών πράξεων

x = MyDual(3)
y = MyDual(6)

@show x
@show y

@show x + y
@show x - y
@show x * y
@show y/x


# Ορισμός των συναρτήσεων sin, cos, tan, exp, log, abs, cosh
# για dual number όρισμα


@inline sin(x::MyDual) = MyDual(sin(x.a), cos(x.a) * x.b)
@inline cos(x::MyDual) = MyDual(cos(x.a), sin(x.a) * (-x.b))
@inline tan(x::MyDual) = MyDual(tan(x.a), x.b .* sec(x.a).^2);

@inline exp(x::MyDual) = MyDual(exp(x.a), exp(x.a)*x.b)
@inline log(x::MyDual) = MyDual(log(x.a), x.b/x.a)

@inline abs(x::MyDual) = MyDual(abs(x.a), x.b*sign(x.a))

@inline cosh(x::MyDual) = MyDual(cosh(x.a), x.b .* sinh(x.a));


function promote_rule(::Type{MyDual{T}}, ::Type{MyDual{R}}) where {T,R} 
    MyDual{promote_type(T,R)}
end
  
function promote_rule(::Type{MyDual{T}}, ::Type{R}) where {T,R}
    MyDual{promote_type(T,R)}
end

function convert(::Type{MyDual{T}}, x::MyDual) where T
    MyDual(convert(T, x.a), convert(T, x.b))
end

function convert(::Type{MyDual{T}}, x::Real) where T
    MyDual(convert(T, x), zero(T))
end


derivative(f, x) = f(MyDual(x)).b


# Παράδειγμα χρήσης

f(x) = x^3-2x^2+5x+1

df(x) = derivative(f, x)

df_analytic(x) = 3x^2-4x + 5

x=LinRange(-10, 10, 200)


fig1 = plot(x, df_analytic.(x), linestyle=:solid, color=:black,
     label = "analytic",
     framestyle = :box,
     grid = (:all, :blue, :dot, 1, 0.3))
plot!(x, df.(x), linestyle=:dash, alpha=0.5,
     label = "using MyDual", color = :red,
     linewidth=5)
xlabel!("x")
ylabel!(L"\frac{df}{dx}")

display(fig1)



f2(x) = sin(x)
df2(x) = derivative(sin, x)
df2_analytic(x) = cos(x)

x = LinRange(0,4π, 401)

fig2 = plot(x, df2_analytic.(x), linestyle=:solid, color=:black,
     label = "analytic",
     framestyle = :box,
     grid = (:all, :blue, :dot, 1, 0.3))
plot!(x, df2.(x), linestyle=:dash, alpha=0.5,
     label = "using MyDual", color = :red,
     linewidth=5)
xlabel!("x")
ylabel!(L"\frac{df}{dx}")

display(fig2)

x = LinRange(-2, 2, 201)


f3(x) = 4*exp(2x)
df3(x) = derivative(f3,x)
df3_analytic(x) = 8*exp(2*x)

fig3 = plot(x, df3_analytic.(x), linestyle=:solid, color=:black,
     label = "analytic",
     framestyle = :box,
     grid = (:all, :blue, :dot, 1, 0.3))
plot!(x, df3.(x), linestyle=:dash, alpha=0.5,
     label = "using MyDual", color = :red,
     linewidth=5)
xlabel!("x")
ylabel!(L"\frac{df}{dx}")
display(fig3)


x = LinRange(0,π, 401)


# f4(x) = exp(cos(x))
# df4_analytic(x) = -sin(x)*exp(cos(x))


f4(x) = cos(exp(x))

df4_analytic(x) = -sin(exp(x))*exp(x)
df4(x) = derivative(f4,x)



fig4 = plot(x, f4.(x), linestyle=:solid, color=:blue,
            label = L"f(x)",
            framestyle = :box,
            grid = (:all, :blue, :dot, 1, 0.3))
plot!(x, df4_analytic.(x), linestyle=:solid, color=:green,
     label = L"analytic \frac{df}{dx}")
plot!(x, df4.(x), linestyle=:dash, alpha=0.5,
     label = L"MyDual \frac{df}{dx}", color = :red,
     linewidth=5)
xlabel!("x")
ylabel!(L"\frac{df}{dx}")
display(fig4)


