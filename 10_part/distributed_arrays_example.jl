using Distributed
using DistributedArrays

if nworkers()>1
    rmprocs(workers());
end

n_processes = 16
addprocs([("userName@server1", n_processes)]; tunnel=true, exename="/home/userName/.juliaup/bin/julia",  exeflags="--project=/home/userName/source/julia/10_part", dir="/home/userName/source/julia/10_part", shell=:posix)

@everywhere include("code_everywhere.jl")

@everywhere @show Base.active_project()

x_disrtibuted, y_disrtibuted, k_disrtibuted = waveNumber_distributed(0.2, 100, 100);

const σ = 0.2
nx = 1000
ny = 1000

println("Memory needed for the matrix of waveNumbers = $(nx*ny*8/1024/1024)MB")

@time x, y, k_distributed = waveNumber_distributed(0.2, nx, ny);

# k = Matrix(k_distributed);
k = convert(Matrix{Float64},k_distributed)

close(k_distributed)

if nworkers()>1
    rmprocs(workers())
end