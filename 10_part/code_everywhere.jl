using Distributed
using DistributedArrays

using Roots
using Pkg


function waveNumber(σ,h)

    if  h < eps(1.)
       return -99.
    end
    
    k₀h=σ^2*h/9.81 # kh for deep water
    
    f(x) = k₀h/x − tanh(x)
    df(x) = −k₀h/x^2 + tanh(x)^2 − 1
    
    k₀h = k₀h/sqrt(tanh(k₀h)) # Use of Eckart (1951) approximate wave theory
    
    find_zero((f,df), k₀h, Roots.Newton())/h;  # Iterate using Newton's method

end

mypeaks(x, y) = 2*(1-x)^2*exp(-(x^2) - (y+1)^2) - 5*(x/5 - x^3 - y^5)*exp(-x^2-y^2) - 1/2*exp(-(x+1)^2 - y^2)

mybathymetry(x, y) = 20 + mypeaks(x, y)

meshgrid(x, y) = ([x for _ in y, x in x], [y for y in y, _ in x])

function waveNumber!(waveNumbers, xl, yl, σ)

     #local_rows, local_columns = localindices(waveNumbers);
    # println("local indices of waveNumbers: ($local_rows, $local_columns) ")

    
    local_waveNumbers = Matrix{Float64}(undef, length(yl), length(xl) )

    for ix in eachindex(xl)
        for iy in eachindex(yl)
            local_waveNumbers[iy, ix] = waveNumber(σ, mybathymetry(xl[ix], yl[iy]) )
        end
    end

    waveNumbers[:L]  = local_waveNumbers

end

function waveNumber_distributed(σ, nx, ny)

    x = collect(LinRange(-4, 4, nx))
    y = collect(LinRange(-4, 4, ny))

    waveNumbers = dzeros(ny, nx)

    @sync for id in workers()
        ic = findall(x->x==id, waveNumbers.pids)         
        local_rows, local_columns = waveNumbers.indices[ic][1]
        @async @fetchfrom id waveNumber!(waveNumbers, x[local_columns], y[local_rows], σ)
    end
    
    return x, y, waveNumbers    
end 

#@show Base.active_project()
