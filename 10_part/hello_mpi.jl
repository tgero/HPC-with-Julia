using MPI
MPI.Init()

comm = MPI.COMM_WORLD
print("Hello, I am process with rank $(MPI.Comm_rank(comm)) of $(MPI.Comm_size(comm))\n")
MPI.Barrier(comm)
