### A Pluto.jl notebook ###
# v0.19.25

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 969d3091-9e03-435f-8317-2b7bb124b5e7
begin
	using Base.Threads

	using StaticArrays
	
	using BenchmarkTools
	using LinearAlgebra

	import Transducers
	import ThreadsX
	
	using PlutoUI

end

# ╔═╡ 142814a0-e29c-11ed-01df-b9da3b03af67
begin
	
end

# ╔═╡ 8ecfd1c6-3529-4004-9b2e-dcc02be6b673
nthreads()

# ╔═╡ ae90e6c7-5ce9-415c-b5f0-07cea41682ec
md"""
### Στοιχεία συναρτησιακού προγραμματισμού
"""

# ╔═╡ 031cea41-54c5-4b9e-9cb6-81683074cc59
md"""
#### Pure functions

Καθαρή συνάρτηση (pure function) είναι μια συνάρτηση της οποίας η έξοδος καθορίζεται πλήρως από την είσοδό της.
"""

# ╔═╡ 4b4c83ce-380f-4b66-8130-07a7a1c8582c
f(x) = x^2+2x+1

# ╔═╡ 052c9aaf-7168-4ded-b817-be3b00eabf87
f(2)

# ╔═╡ 2963e1ed-3e74-43c5-9db4-efb56dc5bebb
md"""
Αντιπαράδειγμα 1ο:
"""

# ╔═╡ 37b85e46-4963-4875-892b-e3fadae9fdd3
g(x) = x + rand()

# ╔═╡ 1874dc0b-d08e-45bd-97b9-e18a89b6b69a
md"""
Η g δεν δίνει το ίδιο αποτέλεσμα για τα ίδια ορίσματα.
"""

# ╔═╡ c8e55857-182e-4432-ae28-8616c6cfc6e3
md"""
Αντιπαράδειγμα 2ο:
"""

# ╔═╡ 152e6be5-667f-4158-a1f0-53e869a43f87
function f2!(x) 
	x[1] = 2
	x[2] = 4
	x
end

# ╔═╡ 0f55103d-46cd-46a2-873f-81e286ec4f53
begin
	x=[5,8]
	@show x
	f2!(x)	
	@show x
end


# ╔═╡ fad10905-04fb-492b-ba41-6d5e41dca925
md"""
Η f2! μεταλλάσσει (mutates) το διάνυσμα x
"""

# ╔═╡ 76b75518-75b6-462c-a699-8ed866ee29e4
md"""
Η χρήση pure function περιορίζει την δημιουργία λογικών λαθών λόγω μεταλλάξεων και διευκολύνει τον compiler στη βελτιστοποίηση  του κώδικα. Επιτρέπει επίσης τον παραλληλισμό του κώδικά όταν η καθαρή συνάρτηση εφαρμόζεται σε αριθμήσιμο  τύπο δεδομένων, π.χ. σε ένα array
"""

# ╔═╡ ebfdbee0-8388-4deb-9eab-86cfee4d62fb
collect(LinRange(0, 2π, 10000))

# ╔═╡ 105206fd-417d-4431-9436-8f4194dc81bf
begin
	xi = collect(LinRange(0, 2π, 10000))
end

# ╔═╡ f94fd12e-1f84-45a8-affc-f512f4126a8e
# @btime yi = map(f, $xi)
@time yi = map(f, xi)

# ╔═╡ c65b4746-e8be-474c-98ea-990cd583ec1c
md"""
Αντιπαράδειγμα 3ο:
"""

# ╔═╡ 1fe31570-a411-4538-84e8-1d374f86d1f2
begin
	 a1 = 3.
	 a2 = 1.
end

# ╔═╡ 4b21e96f-6bf8-4e90-bccc-60d810c6bf42
f3(x) = x^2 + a1*x + a2

# ╔═╡ 30870cec-639b-4405-b3ab-1a6a0e5ad188
# @btime yi3 = map(f3, $xi)
@time yi3 = map(f3, xi)

# ╔═╡ d5e514f6-41e1-4770-acf5-021f3cc247ab
md"""
### Higher Order Functions (Συναρτήσεις Ανώτερης Τάξης)

Δέχεται ως όρισματα συναρτήσεις και μπορεί να επιστρέφει συναρτήσεις. Π.χ.:

"""

# ╔═╡ 831d66d2-f7e6-46ea-a055-a8d654866891
apply_twice(fn::Function, x::Number) = fn(fn(x))

# ╔═╡ ca069098-e89b-41ba-88dc-cca1334e24e6
my_function = x -> x+1

# ╔═╡ 088f2213-3493-4305-b63b-4fedba83397c
apply_twice(fn::Function) = fn∘fn

# ╔═╡ b22dd08c-16e2-4210-8c94-1271ff9aff98
apply_twice( my_function , 1.)

# ╔═╡ 5051c921-ef72-4c70-b239-3e406c124003
apply_twice(my_function)(1)

# ╔═╡ f524e9e3-c62b-49bf-9690-41305d004a2a
md"""
#### Συνήθεις build in Higher Order Functions
"""

# ╔═╡ 2ff46b0c-ce50-41b6-8cac-81364f98a805
#@bind ni Slider( 6:8, default=6)
ni = 6  # Για τιμές μεγαλύτερες του 8 θα χρειαστεί μνήμη μεγαλύτερη από 8Gb

# ╔═╡ 26959668-47ad-496b-8e6f-2e7f6baf50dc
println("ni = $ni")

# ╔═╡ 0805dda8-7c46-4567-b2f4-51483f498321
vals = rand(LinRange(-100,100,201),10^ni)

# ╔═╡ 70306e23-5056-4557-b24f-1eefdd4ade0e
md"""
#### filter
"""

# ╔═╡ 58beb67f-ec56-46bc-b120-f9a0df900fa4
begin
	ispositive(x) = x > 0
	filter(ispositive, vals)
end

# ╔═╡ 9deb0109-e938-45c7-86c8-32be7661d7be
filter(x -> x > 0, vals)

# ╔═╡ dd6d4d12-d390-451e-8f24-465ee6bba62f
md"""
#### count
"""

# ╔═╡ 1ba0254a-1754-47c9-9dfc-1bb74116bffd
count(x -> x > 0, vals)

# ╔═╡ abd79c4f-ab33-4481-9e94-f1d0751b7a4b
md"""
#### map
"""

# ╔═╡ d777d7b8-81f4-495a-92c8-e614bfe3581d
 #@btime map(f, $vals)
 @time map(f, vals)

# ╔═╡ 604b4346-5bfb-4741-ac4b-a58ef0edc4c0
# @btime f.($vals)
@time f.(vals)

# ╔═╡ 3cf6a09f-8124-4634-b7ff-bcf7a933ec5d
md"""
#### reduce
"""

# ╔═╡ 7abc5555-fba2-4a77-942b-0449af984de2
# @btime reduce(+, $vals)
@time reduce(+, vals)

# ╔═╡ 5086e392-8fe2-498d-84e7-5eecbc51ad7b
# @btime sum($vals)
@time sum(vals)

# ╔═╡ b53f636b-762f-4acb-9d94-945961553828
md"

**Προσοχή:** στην εφαρμογή της reduce(f, x) η συνάρτηση f που εφαρμόζεται πρέπει να ικανοποιεί την προσεταιριστική ιδιότητα:

$f(f(a,b), c) = f(a, f(b, c))$

π.χ. για την + :

$a + (b + c) = (a + b) + c$

"

# ╔═╡ 177dcc8c-e9e6-44f6-bc32-3d77a36086ee
begin
	g(x,y) =  x<y ? x : y
	myminimum(vals) = reduce(g, vals)
end

# ╔═╡ 02013137-819a-462b-8187-058986073482
g(1), g(2)

# ╔═╡ 86af1c8f-943e-498a-bd03-4ba03df67092
g(1), g(2)

# ╔═╡ e4a40994-750a-440b-b609-7088a3b3a595
# @btime myminimum($vals)
@time myminimum(vals)

# ╔═╡ 49e1dae4-27bb-4886-b9f5-f2f6082cf82f
# @btime minimum($vals)
@time minimum(vals)

# ╔═╡ 770e4997-5093-4d9a-93d0-1f17994c2751
reduce(-, vals)

# ╔═╡ 036fae18-39bd-4172-b939-961e687a77f6
md"""
#### foldl, foldr
"""

# ╔═╡ baf0aa4d-da79-4cbd-a3f9-4eb3b9b8ce30
foldl(-, vals)

# ╔═╡ 40f8c111-068f-4509-97de-bf73ffd990f4
foldr(-, vals)

# ╔═╡ 572f0eb1-0371-4b53-aee8-c321be69741c
md"""
#### mapreduce
"""

# ╔═╡ fc7fe19c-e0cd-42fb-937c-df283df79a78
mapreduce(x->x^2, +,  vals)

# ╔═╡ 0b5e0a52-12b7-4a14-80cb-215d241db7ef
myrms(vals) = sqrt(mapreduce(x->x^2, +,  vals)/length(vals))

# ╔═╡ 6dc676c4-71bb-4245-9bdd-6ea6c5bc7cd7
# @btime myrms($vals)
@time myrms(vals)

# ╔═╡ d82b31ca-ad37-48fa-b533-4b8f1cf70871
myrms2(x) = norm(x) / sqrt(length(x))

# ╔═╡ 9bec8c9d-1aee-44e2-a65d-30d9a512f927
# @btime myrms2($vals)
@time myrms2(vals)

# ╔═╡ 511c2c89-dec1-476b-a8b5-153d0c9c98bb
function myrms3(x)
	temp = zero(eltype(x))

	for xi in x
		temp += xi^2
	end

	sqrt(temp/length(x))
end

# ╔═╡ 96e75621-1c3d-4e4d-b4d6-c77157f91163
# @btime myrms3($vals)
@time myrms3(vals)

# ╔═╡ 03bdfc7e-5d16-45bc-ab46-00b6d2e954fc
function myrms4(x)
	temp = zero(eltype(x))
	@inbounds @simd for xi in x
		temp += xi^2
	end

	sqrt(temp/length(x))
end

# ╔═╡ 41562ad6-3f0b-417d-86c8-070f3438f73e
# @btime myrms4($vals)
@time myrms4(vals) 

# ╔═╡ fab5904f-f8bf-4971-b28f-ea3bccf229ab
md"""
### Closures
"""

# ╔═╡ 2956f69d-3b00-447b-825f-0d7f8a5a1dc7
function trinomial(a, b, c)

   count = 0
	
   function f(x)	   
	   count+=1
	   println("You called me $count times!")

	   a*x^2 + b*x + c
   end
end

# ╔═╡ 522d780e-a46c-40b3-9aab-aced89b12248
begin
 trion1 = trinomial(1, -2, 3)
 trion1(2)
 trion1(4)
 trion1(5)
end

# ╔═╡ 492fac43-ad42-40b1-8d68-f72b02683627
begin
 trion2 = trinomial(3, 2, 3)
 trion2(2)	
 trion2(4)	
end

# ╔═╡ b5794485-0b5b-49be-b74a-0c90e0bcc9a2
md"""
### Αλυσίδα συναρτήσεων
"""

# ╔═╡ 62d104d3-f026-49a6-b942-932165655c76
text = "Η_ζωή_είναι_ωραία"

# ╔═╡ 0c71dc47-c6f8-4087-a3eb-a0f18f689620
text2 = split(text, "_")

# ╔═╡ 2e026b0e-8e9a-40fb-8c56-d3f2828eb303
text3 = map(uppercasefirst, text2)

# ╔═╡ 82180796-8f10-409a-a3c7-d6a47348effd
"Theo "*"Gerostathis "

# ╔═╡ b9f49099-da1b-4d7b-8419-a6294fa4bf0a
text4 = map(x->*(x," "), text3)

# ╔═╡ b3d33c68-a380-432f-aec1-579d85c490a8
text_final = join(text4)

# ╔═╡ 93277598-b7c2-4e21-9e09-78f75417a539
begin 
	κόψε = text -> split(text, "_")
	πρώτο_κεφαλαίο =text -> map( uppercasefirst, text)
	κενό_στο_τέλος = text -> map(text ->  *(text," "), text)
	ένωσε = text -> join(text)
end

# ╔═╡ f654e0bf-e165-489b-9589-303206fee900
 ένωσε(κενό_στο_τέλος(πρώτο_κεφαλαίο(κόψε(text))))

# ╔═╡ 15a3e2bd-242b-4b93-a824-f7ebfb4b5a8b
text |> κόψε |> πρώτο_κεφαλαίο |> κενό_στο_τέλος |> ένωσε

# ╔═╡ da7a3dfa-bbe5-4f00-bc11-8251c3aec2d5
(ένωσε ∘ κενό_στο_τέλος ∘ πρώτο_κεφαλαίο ∘ κόψε)(text)

# ╔═╡ 27b8900e-7cbb-4814-a9c6-bf980c157eb6
επεξεργαστής_κειμένου = ένωσε ∘ κενό_στο_τέλος ∘ πρώτο_κεφαλαίο ∘ κόψε;

# ╔═╡ d47e22d2-d878-4ff5-8526-a11ba26f7276
επεξεργαστής_κειμένου(text)

# ╔═╡ 93ae8325-0edf-460c-a5a1-24f959456322
md"""
### Συναρτησιακός προγραμματισμός και παράλληλη επεξεργασία
"""

# ╔═╡ 54c24091-77d2-4a8f-9ed1-70690b7d72a3
function multithreading_rms(x)
	temps = zeros(eltype(x), nthreads())
	@threads for i in eachindex(x)
		 @inbounds temps[threadid()] += x[i]^2
	end

	s = zero(eltype(x))
	
	@inbounds @simd for i in eachindex(temps)
		 s += temps[i]
	end	
	
	sqrt(s/length(x))
end

# ╔═╡ bda9c0ad-5974-421e-8fd4-2d9f51d9ffb1
# @btime multithreading_rms($vals)
@time multithreading_rms(vals)

# ╔═╡ 8763b53c-2b8f-45b3-9c70-c17b9991751c
multithreading_rms2(vals) = sqrt( Transducers.foldxt(+, Transducers.Map(x->x^2), vals)/length(vals))

# ╔═╡ eb7b25e7-ec8c-4c31-a1b6-0ed17594c7ee
# @btime multithreading_rms2($vals)
@time multithreading_rms2(vals)

# ╔═╡ b3238683-d3e1-43cb-8081-2c33adafeed2
multithreading_rms3(vals) = sqrt(ThreadsX.mapreduce(x->x^2, +,  vals)/length(vals))

# ╔═╡ 02c505b8-2cd5-41f1-94af-fdcc41ea0d69
# @btime multithreading_rms3($vals)
@time multithreading_rms3(vals)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"
ThreadsX = "ac1d9e8a-700a-412c-b207-f0111f4b6c0d"
Transducers = "28d57a85-8fef-5791-bfe6-a80928e7c999"

[compat]
BenchmarkTools = "~1.3.2"
PlutoUI = "~0.7.50"
StaticArrays = "~1.5.23"
ThreadsX = "~0.1.11"
Transducers = "~0.4.75"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.9.0"
manifest_format = "2.0"
project_hash = "8aad0c3dec587989290c133539d07a9571fb2e24"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.Adapt]]
deps = ["LinearAlgebra", "Requires"]
git-tree-sha1 = "cc37d689f599e8df4f464b2fa3870ff7db7492ef"
uuid = "79e6a3ab-5dfb-504d-930d-738a2a938a0e"
version = "3.6.1"
weakdeps = ["StaticArrays"]

    [deps.Adapt.extensions]
    AdaptStaticArraysExt = "StaticArrays"

[[deps.ArgCheck]]
git-tree-sha1 = "a3a402a35a2f7e0b87828ccabbd5ebfbebe356b4"
uuid = "dce04be8-c92d-5529-be00-80e4d2c0e197"
version = "2.3.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"
version = "1.1.1"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.BangBang]]
deps = ["Compat", "ConstructionBase", "Future", "InitialValues", "LinearAlgebra", "Requires", "Setfield", "Tables", "ZygoteRules"]
git-tree-sha1 = "7fe6d92c4f281cf4ca6f2fba0ce7b299742da7ca"
uuid = "198e06fe-97b7-11e9-32a5-e1d131e6ad66"
version = "0.3.37"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Baselet]]
git-tree-sha1 = "aebf55e6d7795e02ca500a689d326ac979aaf89e"
uuid = "9718e550-a3fa-408a-8086-8db961cd8217"
version = "0.1.1"

[[deps.BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "d9a9701b899b30332bbcb3e1679c41cce81fb0e8"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.3.2"

[[deps.ChainRulesCore]]
deps = ["Compat", "LinearAlgebra", "SparseArrays"]
git-tree-sha1 = "c6d890a52d2c4d55d326439580c3b8d0875a77d9"
uuid = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
version = "1.15.7"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "eb7f0f8307f71fac7c606984ea5fb2817275d6e4"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.4"

[[deps.Compat]]
deps = ["UUIDs"]
git-tree-sha1 = "7a60c856b9fa189eb34f5f8a6f6b5529b7942957"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.6.1"
weakdeps = ["Dates", "LinearAlgebra"]

    [deps.Compat.extensions]
    CompatLinearAlgebraExt = "LinearAlgebra"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "1.0.2+0"

[[deps.CompositionsBase]]
git-tree-sha1 = "455419f7e328a1a2493cabc6428d79e951349769"
uuid = "a33af91c-f02d-484b-be07-31d278c5ca2b"
version = "0.1.1"

[[deps.ConstructionBase]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "89a9db8d28102b094992472d333674bd1a83ce2a"
uuid = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
version = "1.5.1"

    [deps.ConstructionBase.extensions]
    IntervalSetsExt = "IntervalSets"
    StaticArraysExt = "StaticArrays"

    [deps.ConstructionBase.weakdeps]
    IntervalSets = "8197267c-284f-5f27-9208-e0e47529a953"
    StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"

[[deps.DataAPI]]
git-tree-sha1 = "e8119c1a33d267e16108be441a287a6981ba1630"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.14.0"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DefineSingletons]]
git-tree-sha1 = "0fba8b706d0178b4dc7fd44a96a92382c9065c2c"
uuid = "244e2a9f-e319-4986-a169-4d1fe445cd52"
version = "0.1.2"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "FileWatching", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"
version = "1.6.0"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
deps = ["Tricks"]
git-tree-sha1 = "c47c5fa4c5308f27ccaac35504858d8914e102f9"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.4"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InitialValues]]
git-tree-sha1 = "4da0f88e9a39111c2fa3add390ab15f3a44f3ca3"
uuid = "22cec73e-a1b8-11e9-2c92-598750a2cf9c"
version = "0.3.1"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "31e996f0a15c7b280ba9f76636b3ff9e2ae58c9a"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.4"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"
version = "0.6.3"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"
version = "7.84.0+0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"
version = "1.10.2+0"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "OpenBLAS_jll", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MIMEs]]
git-tree-sha1 = "65f28ad4b594aebe22157d6fac869786a255b7eb"
uuid = "6c6e2e6c-3030-632d-7369-2d6c69616d65"
version = "0.1.4"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "42324d08725e200c23d4dfb549e0d5d89dede2d2"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.10"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"
version = "2.28.2+0"

[[deps.MicroCollections]]
deps = ["BangBang", "InitialValues", "Setfield"]
git-tree-sha1 = "629afd7d10dbc6935ec59b32daeb33bc4460a42e"
uuid = "128add7d-3638-4c79-886c-908ea0c25c34"
version = "0.1.4"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"
version = "2022.10.11"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"
version = "1.2.0"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.21+4"

[[deps.OrderedCollections]]
git-tree-sha1 = "d321bf2de576bf25ec4d3e4360faca399afca282"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.6.0"

[[deps.Parsers]]
deps = ["Dates", "SnoopPrecompile"]
git-tree-sha1 = "478ac6c952fddd4399e71d4779797c538d0ff2bf"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.5.8"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "FileWatching", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
version = "1.9.0"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "FixedPointNumbers", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "MIMEs", "Markdown", "Random", "Reexport", "URIs", "UUIDs"]
git-tree-sha1 = "5bb5129fdd62a2bbbe17c2756932259acf467386"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.50"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "47e5f437cc0e7ef2ce8406ce1e7e24d44915f88d"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.3.0"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Referenceables]]
deps = ["Adapt"]
git-tree-sha1 = "e681d3bfa49cd46c3c161505caddf20f0e62aaa9"
uuid = "42d2dcc6-99eb-4e98-b66c-637b7d73030e"
version = "0.1.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Setfield]]
deps = ["ConstructionBase", "Future", "MacroTools", "StaticArraysCore"]
git-tree-sha1 = "e2cc6d8c88613c05e1defb55170bf5ff211fbeac"
uuid = "efcf1570-3423-57d1-acb7-fd33fddbac46"
version = "1.1.1"

[[deps.SnoopPrecompile]]
deps = ["Preferences"]
git-tree-sha1 = "e760a70afdcd461cf01a575947738d359234665c"
uuid = "66db9d55-30c0-4569-8b51-7e840670fc0c"
version = "1.0.3"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["Libdl", "LinearAlgebra", "Random", "Serialization", "SuiteSparse_jll"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SplittablesBase]]
deps = ["Setfield", "Test"]
git-tree-sha1 = "e08a62abc517eb79667d0a29dc08a3b589516bb5"
uuid = "171d559e-b47b-412a-8079-5efa626c420e"
version = "0.1.15"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "StaticArraysCore", "Statistics"]
git-tree-sha1 = "c8c6fee2ecfeae4dae754d2b69926d03478d5a1d"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.5.23"

[[deps.StaticArraysCore]]
git-tree-sha1 = "6b7ba252635a5eff6a0b0664a41ee140a1c9e72a"
uuid = "1e83bf80-4336-4d27-bf5d-d5a4f845583c"
version = "1.4.0"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
version = "1.9.0"

[[deps.SuiteSparse_jll]]
deps = ["Artifacts", "Libdl", "Pkg", "libblastrampoline_jll"]
uuid = "bea87d4a-7f5b-5778-9afe-8cc45184846c"
version = "5.10.1+6"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"
version = "1.0.3"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "1544b926975372da01227b382066ab70e574a3ec"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.10.1"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"
version = "1.10.0"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.ThreadsX]]
deps = ["ArgCheck", "BangBang", "ConstructionBase", "InitialValues", "MicroCollections", "Referenceables", "Setfield", "SplittablesBase", "Transducers"]
git-tree-sha1 = "34e6bcf36b9ed5d56489600cf9f3c16843fa2aa2"
uuid = "ac1d9e8a-700a-412c-b207-f0111f4b6c0d"
version = "0.1.11"

[[deps.Transducers]]
deps = ["Adapt", "ArgCheck", "BangBang", "Baselet", "CompositionsBase", "DefineSingletons", "Distributed", "InitialValues", "Logging", "Markdown", "MicroCollections", "Requires", "Setfield", "SplittablesBase", "Tables"]
git-tree-sha1 = "c42fa452a60f022e9e087823b47e5a5f8adc53d5"
uuid = "28d57a85-8fef-5791-bfe6-a80928e7c999"
version = "0.4.75"

[[deps.Tricks]]
git-tree-sha1 = "aadb748be58b492045b4f56166b5188aa63ce549"
uuid = "410a4b4d-49e4-4fbc-ab6d-cb71b17b3775"
version = "0.1.7"

[[deps.URIs]]
git-tree-sha1 = "074f993b0ca030848b897beff716d93aca60f06a"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.4.2"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"
version = "1.2.13+0"

[[deps.ZygoteRules]]
deps = ["ChainRulesCore", "MacroTools"]
git-tree-sha1 = "977aed5d006b840e2e40c0b48984f7463109046d"
uuid = "700de1a5-db45-46bc-99cf-38207098b444"
version = "0.2.3"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.7.0+0"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"
version = "1.48.0+0"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
version = "17.4.0+0"
"""

# ╔═╡ Cell order:
# ╟─142814a0-e29c-11ed-01df-b9da3b03af67
# ╠═969d3091-9e03-435f-8317-2b7bb124b5e7
# ╟─8ecfd1c6-3529-4004-9b2e-dcc02be6b673
# ╟─ae90e6c7-5ce9-415c-b5f0-07cea41682ec
# ╟─031cea41-54c5-4b9e-9cb6-81683074cc59
# ╠═4b4c83ce-380f-4b66-8130-07a7a1c8582c
# ╠═052c9aaf-7168-4ded-b817-be3b00eabf87
# ╟─2963e1ed-3e74-43c5-9db4-efb56dc5bebb
# ╠═37b85e46-4963-4875-892b-e3fadae9fdd3
# ╠═02013137-819a-462b-8187-058986073482
# ╠═86af1c8f-943e-498a-bd03-4ba03df67092
# ╟─1874dc0b-d08e-45bd-97b9-e18a89b6b69a
# ╟─c8e55857-182e-4432-ae28-8616c6cfc6e3
# ╠═152e6be5-667f-4158-a1f0-53e869a43f87
# ╠═0f55103d-46cd-46a2-873f-81e286ec4f53
# ╠═fad10905-04fb-492b-ba41-6d5e41dca925
# ╟─76b75518-75b6-462c-a699-8ed866ee29e4
# ╠═ebfdbee0-8388-4deb-9eab-86cfee4d62fb
# ╠═105206fd-417d-4431-9436-8f4194dc81bf
# ╠═f94fd12e-1f84-45a8-affc-f512f4126a8e
# ╟─c65b4746-e8be-474c-98ea-990cd583ec1c
# ╠═1fe31570-a411-4538-84e8-1d374f86d1f2
# ╠═4b21e96f-6bf8-4e90-bccc-60d810c6bf42
# ╠═30870cec-639b-4405-b3ab-1a6a0e5ad188
# ╟─d5e514f6-41e1-4770-acf5-021f3cc247ab
# ╠═831d66d2-f7e6-46ea-a055-a8d654866891
# ╠═ca069098-e89b-41ba-88dc-cca1334e24e6
# ╠═b22dd08c-16e2-4210-8c94-1271ff9aff98
# ╠═088f2213-3493-4305-b63b-4fedba83397c
# ╠═5051c921-ef72-4c70-b239-3e406c124003
# ╟─f524e9e3-c62b-49bf-9690-41305d004a2a
# ╠═2ff46b0c-ce50-41b6-8cac-81364f98a805
# ╠═26959668-47ad-496b-8e6f-2e7f6baf50dc
# ╠═0805dda8-7c46-4567-b2f4-51483f498321
# ╠═70306e23-5056-4557-b24f-1eefdd4ade0e
# ╠═58beb67f-ec56-46bc-b120-f9a0df900fa4
# ╠═9deb0109-e938-45c7-86c8-32be7661d7be
# ╠═dd6d4d12-d390-451e-8f24-465ee6bba62f
# ╠═1ba0254a-1754-47c9-9dfc-1bb74116bffd
# ╟─abd79c4f-ab33-4481-9e94-f1d0751b7a4b
# ╠═d777d7b8-81f4-495a-92c8-e614bfe3581d
# ╠═604b4346-5bfb-4741-ac4b-a58ef0edc4c0
# ╟─3cf6a09f-8124-4634-b7ff-bcf7a933ec5d
# ╠═7abc5555-fba2-4a77-942b-0449af984de2
# ╠═5086e392-8fe2-498d-84e7-5eecbc51ad7b
# ╟─b53f636b-762f-4acb-9d94-945961553828
# ╠═177dcc8c-e9e6-44f6-bc32-3d77a36086ee
# ╠═e4a40994-750a-440b-b609-7088a3b3a595
# ╠═49e1dae4-27bb-4886-b9f5-f2f6082cf82f
# ╠═770e4997-5093-4d9a-93d0-1f17994c2751
# ╟─036fae18-39bd-4172-b939-961e687a77f6
# ╠═baf0aa4d-da79-4cbd-a3f9-4eb3b9b8ce30
# ╠═40f8c111-068f-4509-97de-bf73ffd990f4
# ╟─572f0eb1-0371-4b53-aee8-c321be69741c
# ╠═fc7fe19c-e0cd-42fb-937c-df283df79a78
# ╠═0b5e0a52-12b7-4a14-80cb-215d241db7ef
# ╠═6dc676c4-71bb-4245-9bdd-6ea6c5bc7cd7
# ╠═d82b31ca-ad37-48fa-b533-4b8f1cf70871
# ╠═9bec8c9d-1aee-44e2-a65d-30d9a512f927
# ╠═511c2c89-dec1-476b-a8b5-153d0c9c98bb
# ╠═96e75621-1c3d-4e4d-b4d6-c77157f91163
# ╠═03bdfc7e-5d16-45bc-ab46-00b6d2e954fc
# ╠═41562ad6-3f0b-417d-86c8-070f3438f73e
# ╟─fab5904f-f8bf-4971-b28f-ea3bccf229ab
# ╠═2956f69d-3b00-447b-825f-0d7f8a5a1dc7
# ╠═522d780e-a46c-40b3-9aab-aced89b12248
# ╠═492fac43-ad42-40b1-8d68-f72b02683627
# ╠═b5794485-0b5b-49be-b74a-0c90e0bcc9a2
# ╠═62d104d3-f026-49a6-b942-932165655c76
# ╠═0c71dc47-c6f8-4087-a3eb-a0f18f689620
# ╠═2e026b0e-8e9a-40fb-8c56-d3f2828eb303
# ╠═82180796-8f10-409a-a3c7-d6a47348effd
# ╠═b9f49099-da1b-4d7b-8419-a6294fa4bf0a
# ╠═b3d33c68-a380-432f-aec1-579d85c490a8
# ╠═93277598-b7c2-4e21-9e09-78f75417a539
# ╠═f654e0bf-e165-489b-9589-303206fee900
# ╠═15a3e2bd-242b-4b93-a824-f7ebfb4b5a8b
# ╠═da7a3dfa-bbe5-4f00-bc11-8251c3aec2d5
# ╠═27b8900e-7cbb-4814-a9c6-bf980c157eb6
# ╠═d47e22d2-d878-4ff5-8526-a11ba26f7276
# ╟─93ae8325-0edf-460c-a5a1-24f959456322
# ╠═54c24091-77d2-4a8f-9ed1-70690b7d72a3
# ╠═bda9c0ad-5974-421e-8fd4-2d9f51d9ffb1
# ╠═8763b53c-2b8f-45b3-9c70-c17b9991751c
# ╠═eb7b25e7-ec8c-4c31-a1b6-0ed17594c7ee
# ╠═b3238683-d3e1-43cb-8081-2c33adafeed2
# ╠═02c505b8-2cd5-41f1-94af-fdcc41ea0d69
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
