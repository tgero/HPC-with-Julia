```@meta
CurrentModule = MyDualNumbers
```

# MyDualNumbers

Documentation for [MyDualNumbers](https://github.com/my_github_username/MyDualNumbers.jl).

```@index
```

```@autodocs
Modules = [MyDualNumbers]
```
