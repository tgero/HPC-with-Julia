using MyDualNumbers
using Documenter

DocMeta.setdocmeta!(MyDualNumbers, :DocTestSetup, :(using MyDualNumbers); recursive=true)

makedocs(;
    modules=[MyDualNumbers],
    authors="My name <user@institution.gr>",
    repo="https://github.com/my_github_username/MyDualNumbers.jl/blob/{commit}{path}#{line}",
    sitename="MyDualNumbers.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://my_github_username.github.io/MyDualNumbers.jl",
        edit_link="master",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)

deploydocs(;
    repo="github.com/my_github_username/MyDualNumbers.jl",
    devbranch="master",
)
