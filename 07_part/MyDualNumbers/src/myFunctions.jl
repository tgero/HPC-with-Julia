@inline +(x::MyDual, y::MyDual) = MyDual(x.a+y.a, x.b+y.b)
@inline -(x::MyDual, y::MyDual) = MyDual(x.a-y.a, x.b-y.b)

# Γινόμενο
@inline *(x::MyDual, y::MyDual) = MyDual(x.a*y.a, y.a*x.b + x.a*y.b)

# Πηλίκο

@inline /(x::MyDual, y::MyDual) = MyDual(x.a / y.a, (x.b*y.a - y.b*x.a) / y.a^2)

@inline sin(x::MyDual) = MyDual(sin(x.a), cos(x.a) * x.b)
@inline cos(x::MyDual) = MyDual(cos(x.a), sin(x.a) * (-x.b))
@inline tan(x::MyDual) = MyDual(tan(x.a), x.b .* sec(x.a).^2);

@inline exp(x::MyDual) = MyDual(exp(x.a), exp(x.a)*x.b)
@inline log(x::MyDual) = MyDual(log(x.a), x.b/x.a)

@inline abs(x::MyDual) = MyDual(abs(x.a), x.b*sign(x.a))

@inline cosh(x::MyDual) = MyDual(cosh(x.a), x.b .* sinh(x.a));

export derivative
derivative(f, x) = f(MyDual(x)).b