module MyDualNumbers

#=

Γεροστάθης Θεόδωρος

Παράδειγμα δημιουργίας custom type για την αυτόματη παραγώγιση συναρτήσεων 
με εφαρμογή των dual numbers.

το παράδειγμα είναι βασισμένο στο άρθρο του Erik Engheim στη σελίδα:

<https://nextjournal.com/erik-engheim/implementation-of-automatic-differentiation>

Επίσης δείτε τα:

https://www.redalyc.org/journal/467/46761359006/html/

https://en.wikipedia.org/wiki/Dual_number

https://en.wikipedia.org/wiki/Automatic_differentiation#Automatic_differentiation_using_dual_numbers


=#

import Base: show, +, -, *, /
import Base: exp, log, abs
import Base: sin, cos, tan
import Base: cosh

import Base: convert, promote_rule

export MyDual

struct MyDual{T<:Real} <: Real
    a::T
    b::T
end

# Constructor με input αριθμό <: Real

MyDual(x) = MyDual(x, one(x))

show(io::IO, x::MyDual) = print(io, x.a, " + ", x.b, "ϵ")


function promote_rule(::Type{MyDual{T}}, ::Type{MyDual{R}}) where {T,R}
    MyDual{promote_type(T, R)}
end

function promote_rule(::Type{MyDual{T}}, ::Type{R}) where {T,R}
    MyDual{promote_type(T, R)}
end

function convert(::Type{MyDual{T}}, x::MyDual) where {T}
    MyDual(convert(T, x.a), convert(T, x.b))
end

function convert(::Type{MyDual{T}}, x::Real) where {T}
    MyDual(convert(T, x), zero(T))
end

include("myFunctions.jl")

end
