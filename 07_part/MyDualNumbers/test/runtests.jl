using MyDualNumbers
using Test

f(x) = x^3-2x^2+5x+1
df(x) = derivative(f, x)
df_analytic(x) = 3x^2-4x + 5

@testset "MyDualNumbers.jl" begin
    # Write your tests here.
    @test df(2.) ≈ df_analytic(2.)    
end
