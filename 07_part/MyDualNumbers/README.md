# MyDualNumbers

[![Stable](https://img.shields.io/badge/docs-stable-blue.svg)](https://my_github_username.github.io/MyDualNumbers.jl/stable/)
[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://my_github_username.github.io/MyDualNumbers.jl/dev/)
[![Build Status](https://github.com/my_github_username/MyDualNumbers.jl/actions/workflows/CI.yml/badge.svg?branch=master)](https://github.com/my_github_username/MyDualNumbers.jl/actions/workflows/CI.yml?query=branch%3Amaster)
