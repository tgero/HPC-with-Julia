using PkgTemplates

# Create a template
# The default plugins are ProjectFile, SrcDir, Tests, Readme, License, Git, CompatHelper, and TagBot. 

t = Template(user = "my_github_username",                            
             authors = "My name <user@institution.gr>",
             dir = ".",
             julia = v"1.9",
             plugins = [
                 ProjectFile(; version=v"0.1.0-DEV"),
                 License(name = "MIT"), # Creates a license file.
                 Git(; ssh=true),       # Creates a Git repository and a .gitignore file.
                 GitHubActions(),       #  Deploys documentation to GitHub Pages with the help of GitHubActions (https://github.com/features/actions).
                 Documenter{GitHubActions}(), # Sets up documentation generation via Documenter.jl.
             ],)

# Create the package
t("MyDualNumbers")
