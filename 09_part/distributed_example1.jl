using Distributed
using Plots

addprocs(8; exeflags="--project=.")

@everywhere include("code_for_everywhere.jl")

x, y, k_shared = waveNumber_shared(0.2, 1000, 1000);

display(contourf(x,y,k_shared, color = :jet, framestyle=:box))

