@show Base.active_project()

using Distributed
using SharedArrays
using Roots

mypeaks(x, y) = 2*(1-x)^2*exp(-(x^2) - (y+1)^2) - 5*(x/5 - x^3 - y^5)*exp(-x^2-y^2) - 1/2*exp(-(x+1)^2 - y^2)
mybathymetry(x, y) = 20 + mypeaks(x, y)

function waveNumber(σ,h)

	if  h < eps(1.)
	   return -99.
	end
	
	k₀h=σ^2*h/9.81 # kh for deep water
    
	f(x) = k₀h/x − tanh(x)
	df(x) = −k₀h/x^2 + tanh(x)^2 − 1
	
    k₀h = k₀h/sqrt(tanh(k₀h)) # Use of Eckart (1951) approximate wave theory
	
    find_zero((f,df), k₀h, Roots.Newton())/h;  # Iterate using Newton's method

end

function waveNumber!(waveNumbers, σ, x, y, j) 
    for i in eachindex(y)
        waveNumbers[i, j] = waveNumber(σ, mybathymetry(x[j], y[i]))
    end
end

function waveNumber_shared(σ, nx, ny)

    x = SharedVector{Float64}(collect(LinRange(-4, 4, nx)))
    y = SharedVector{Float64}(collect(LinRange(-4, 4, ny)))		

    # waveNumbers = SharedArray(Array{Float64,2}(undef,ny, nx))
    waveNumbers = SharedArray{Float64,2}(ny, nx)

    @sync @distributed for j in 1:nx
               waveNumber!(waveNumbers, σ, x, y, j)
    end

    return x, y, waveNumbers
end 
