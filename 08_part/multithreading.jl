### A Pluto.jl notebook ###
# v0.19.26

using Markdown
using InteractiveUtils

# ╔═╡ 94d4817e-fe10-11ed-0c87-03b838e33590
begin

	# 
	import Pkg
	Pkg.activate(Base.current_project())	
	# Κάνετε comment τις δύο παραπάνω εντολές αν θέλετε το Pluto να μην χρησιμοποιήσει το project στο τρέχον folder
	
	using StaticArrays
	using Roots
    using JLD2

	using Base.Threads
	
	import ThreadsX
	
	using  BenchmarkTools	

	using FoldsThreads
	using Folds
	using FLoops
end

# ╔═╡ 3b97f8f7-8afb-4632-9208-e3c7e2d4fe19
function waveNumber(σ,h)

	if  h < eps(1.)
	   return -99.
	end
	
	k₀h=σ^2*h/9.81 # kh for deep water
    
	f(x) = k₀h/x − tanh(x)
	df(x) = −k₀h/x^2 + tanh(x)^2 − 1
	
#    k₀h = k₀h/sqrt(tanh(k₀h)) # Use of Eckart (1951) approximate wave theory
	
    find_zero((f,df), k₀h, Roots.Newton())/h;  # Iterate using Newton's method

end

# ╔═╡ 837d0b2f-ab8e-44be-b172-6774ee535f40
 waveNumber(0.2,10)

# ╔═╡ 92cebf2c-eb11-42dd-809c-b151d151e77b
begin
	bathymetry_file = jldopen("bathymetry.jdl2", "r");
	lon = bathymetry_file["lon"];	
	lat = bathymetry_file["lat"];	
	depth = Float64.(bathymetry_file["bathymetry"];)
end;

# ╔═╡ fcd842ab-5ee5-4217-910e-e6a8d6e6ab5b
f = x->waveNumber(0.2, x)

# ╔═╡ 8bed2cde-d387-4c43-9daf-2c068af7f4cf
@benchmark k =  map($f, $depth)

# ╔═╡ 3c2e58db-5b3e-46c0-a19a-829104692c33
K1 = similar(depth);

# ╔═╡ 8fe084bb-da68-45a6-965d-a2b1fdfb214e
time1 = @benchmark ($K1) .= f.($depth)

# ╔═╡ 05e42c42-536a-443d-bd67-e8bc85d8ec46
serial_time = median(time1.times) # time in ns

# ╔═╡ 88cf22c3-6271-4984-9c6e-d0eeebbf6cbe
nthreads()   # Η τιμή της μεταβλητής περιβάλλοντος JULIA_NUM_THREADS είναι ίση με 8

# ╔═╡ 7a4a13ec-9d3a-41b5-8aa4-4f04172acc30
time2 = @benchmark k0 =  ThreadsX.map($f, $depth)

# ╔═╡ cbbd21ed-95ca-400c-8525-1c060856ac70
ThreadsX_time = median(time2.times) # time in ns

# ╔═╡ c4c3a5f2-66ae-4159-8808-51ff502e5467
println("ThreadsX speed up : $( serial_time/ThreadsX_time)")

# ╔═╡ 36a310f3-5d16-4874-adf6-3c484ca4dad5
function kh_floops!(K, ω, depth)
	FLoops.@floop for i in eachindex(depth)
		K[i] = waveNumber(ω, depth[i])
	end
end

# ╔═╡ a69f5a2c-e7c4-403e-9c0c-2b6a33d56d9d
K2 = similar(depth);

# ╔═╡ 76f7f4c4-8dd5-4411-bd07-751544fc0b97
time7 = @benchmark  kh_floops!($K2, 0.2, $depth)

# ╔═╡ 6f46494e-5546-4db2-98cf-9b9186a965da
floop_time =  median(time7.times) # time in ns

# ╔═╡ 83a3f281-2289-46a8-8409-ae5c3e28227d
println("@floop speed up : $( serial_time/floop_time)")

# ╔═╡ 5955a2b4-6369-4a77-9feb-7138924ee5e5
time8 = @benchmark K22 = Folds.map(f, depth,  DepthFirstEx() )

# ╔═╡ 9bd7ffbc-2167-4f6f-9683-56434e696d3b
folds_map_time =  median(time8.times) # time in ns

# ╔═╡ 1c0fa66c-f734-4213-bce5-a67e6af397be
println(" Folds.map speed up : $( serial_time/folds_map_time)")

# ╔═╡ ac2fe33e-3432-4c46-a41e-fdaf86f22eb6
md"
### Threads in a loop
"

# ╔═╡ 39083364-d3fd-45b4-90c3-11567143239a
function kh!(K, ω, depth)
	@inbounds Threads.@threads for i in eachindex(depth)
		K[i] = waveNumber(ω, depth[i])
	end
end

# ╔═╡ 1ee486c7-b6f2-4e9e-8b7b-cefbb9e5d0fc
time3 = @benchmark kh!($K2, 0.2, $depth)

# ╔═╡ f3dc2dc6-8ff9-4611-a85a-c972bc465290
threads_macro_time = median(time3.times) # time in ns

# ╔═╡ f175f9fd-5fb2-4f30-91a2-6e1c89852c3d
println("Threads.@threads speed up : $( serial_time/threads_macro_time)")

# ╔═╡ a77d9868-7a61-466d-a0aa-12c219fc4707
waveNumber(0.2,10)

# ╔═╡ adac024f-2124-427a-b59f-024097ce156e
md"
### Tasks
"

# ╔═╡ aa969665-4afe-47ae-b9eb-53be8050b7ef
 begin 
	task1 = @spawn waveNumber(0.2,15)
	task2 = @spawn waveNumber(0.2,12)
	task3 = @spawn waveNumber(0.2,2)
end


# ╔═╡ 89428484-bc90-4ea3-9c51-e5155bf75e86
fetch(task2), fetch(task3), fetch(task1)

# ╔═╡ 23ba3460-e68c-4bd4-aa80-bfa4b7148d0c
function kh_spawn!(K, ω, depth)
	@inbounds @sync for j in axes(depth, 2)		
		@spawn 	for i in axes(depth, 1) 
			K[i, j] = waveNumber(ω, depth[i, j])
				end
	end
end

# ╔═╡ e26febfa-d4b9-4304-bd4b-944d41615f77
time4 = @benchmark  kh_spawn!($K2, 0.2, $depth)

# ╔═╡ f6a3068e-90c8-45bd-bcbb-499c604e6a75
spawn_time =  median(time4.times) # time in ns

# ╔═╡ 376f6b50-bd6f-4414-b754-f706374deb4d
println("@spawn speed up : $( serial_time/spawn_time)")

# ╔═╡ 583e9e4b-aebe-4359-a676-3cecce1371d5
md"
### Multithreading σε αναδρομικές συναρτήσεις
"

# ╔═╡ 1797cbaf-54de-4aa4-9a23-bfc4491b5ea9
function trapz(f::Function, a::Float64, b::Float64, n::Int64)
    dx = (b-a)/n
    y = (f(a) + f(b))/2
    x = a
    for i=1:n-1
        x = x + dx
        y = y + f(x)
    end
    return dx*y
end

# ╔═╡ ce915cf7-0dd4-4c12-af70-c1b2be6718d0
int_exact = exp(1) - 1.0

# ╔═╡ 43307b99-63a0-41c3-b893-6ffc3c1a09df
n=10^8

# ╔═╡ 431d6ef7-10c9-4dac-8f8a-ecb253847276
abs(trapz(exp, 0., 1., n) - int_exact)

# ╔═╡ d793b58d-e3fa-4253-b901-4eaef0548ffb
time5 = @benchmark  trapz(exp, 0., 1., n)

# ╔═╡ 33307e72-8fa3-4a1c-b5ea-7093ce98b7b1
serial_integral_time = median(time5.times)

# ╔═╡ ea18a8fe-6c99-4338-ab25-a34c7e4a7bd7
function trapz_recursive(current_level::Int64, final_level::Int64,
	                     f::Function, a::Float64, b::Float64, n::Int64)
	    if current_level == final_level
	        return trapz(f, a, b, n)
	    else
	        middle = (b-a)/2.
	
	        task = @spawn trapz_recursive(current_level+1,final_level, f, a, middle, div(n, 2))
	        return trapz_recursive(current_level+1,final_level, f, middle, b, div(n, 2)) + fetch(task)
	    end
end

# ╔═╡ bc33caf2-5846-4db0-8533-ba3144a25d7d
abs(trapz_recursive(0, 3, exp, 0., 1., n) - int_exact)

# ╔═╡ a5f030ab-b63d-47cb-980f-2fd66f3e8e83
time6 = @benchmark trapz_recursive(0, 3, exp, 0., 1., n)

# ╔═╡ 7e1a0d09-69c9-4225-9962-5a8031d92ba4
multithreading_integral_time = median(time6.times)

# ╔═╡ 5090a103-100e-4ac3-a465-680da04c4ebb
println("Multithreading speed up : $( serial_integral_time/multithreading_integral_time)")

# ╔═╡ 817b2063-971c-4a2d-8748-2bd6f75e4e90
md"
### Συνάρτηση με race condition
"

# ╔═╡ f9a7b44d-716a-45d7-b4c3-361fb271cea2
function rms_serial(x)
	temp = zero(eltype(x))
	@inbounds for xi in x
		temp += xi^2
	end

	sqrt(temp/length(x))
end

# ╔═╡ 911bf409-519e-4dc7-9572-22119b2a5e6f
ni=6

# ╔═╡ aa8c6863-8d63-4bf0-88da-7f1116173502
LinRange(-100,100,201)

# ╔═╡ 43853644-248d-4307-a8fc-82ffb812a990
vals = rand(LinRange(-100,100,201), 10^ni)

# ╔═╡ 4a274bab-ede4-4191-bb4c-bbb66f37bb31
rms_result = rms_serial(vals)

# ╔═╡ d208d8ca-f8c2-4c1b-b967-3f300c098aa0
timestruct_serial = @benchmark rms_serial($vals)

# ╔═╡ 0ec3889d-d204-494f-b8dc-9fa8c7dbe6f5
rms_serial_time = median(timestruct_serial.times)

# ╔═╡ d7669c3f-0194-4932-b760-aee5b69eed8c
md"
Όταν έχουμε race condition oι υπολογισμοί δεν είναι ασφαλείς και το αποτέλεσμα είναι τυχαίο.
"

# ╔═╡ 8b111fb6-494e-447c-bbcd-4a282ee1da3a
function rms_multithreading_with_race_condition(x)
	temp = zero(eltype(x))
	@inbounds @threads for i in eachindex(x)
		temp += x[i]^2
	end

	sqrt(temp/length(x))
end

# ╔═╡ 8d5cdebc-9d06-4b9c-8806-db2f41d3ee3d
rms_multithreading_with_race_condition(vals)  # Εκτελέστε την συνάρτηση πολλές φορές. Κάθε φορά δίνει διαφορετικό αποτέλεσμα

# ╔═╡ 7c4c2d07-651b-48dd-bb24-1ac65b045558
rms_multithreading_with_race_condition(vals) ≈ rms_result

# ╔═╡ 25f93517-0714-4a08-a78c-9deb6aab92eb
md"
### Αντιμετώπιση της race condition με χρήση atomic (πολύ αργή λύση...)
"


# ╔═╡ a86f69e5-2bde-4c59-96e7-e4f42e62a3ee
function rms_multithreading_with_atomic(x)
	
	temp = Atomic{eltype(x)}(zero(eltype(x)))
	@threads for i in eachindex(x)
		@inbounds atomic_add!(temp, x[i]^2)
	end
    
	sqrt(temp[]/length(x))
end

# ╔═╡ 2991d7cc-5fe3-4dc1-8e1c-85420f9ed82f
rms_multithreading_with_atomic(vals)

# ╔═╡ cf153a5c-2805-417a-b460-19325ad4b37a
rms_multithreading_with_atomic(vals) ≈ rms_result

# ╔═╡ 077791d1-710d-4f23-a7af-a6d2327d1b99
timestruct_multithreading_with_atomic = @benchmark rms_multithreading_with_atomic($vals) # μέχρι ni = 6

# ╔═╡ 8a21233a-5ef1-4b01-91c4-9640aedeed2a
md"
### Αντιμετώπιση της race condition με τοπική άθροιση σε διαφορετική θέση μνήμης
"

# ╔═╡ a6f2575c-ea7b-4aa4-8afd-12ed9f7c986b
function rms_multithreading(x)
	
	temps = zeros(eltype(x), nthreads())
	
	@threads for i in eachindex(x)
		 @inbounds temps[threadid()] += x[i]^2
	end

	sqrt(sum(temps)/length(x))
end

# ╔═╡ f09486c4-f575-471f-8e35-5e8c5d8e9646
rms_multithreading(vals)

# ╔═╡ 886624b3-264e-4423-a587-64e59433dc0a
rms_multithreading(vals) ≈ rms_result

# ╔═╡ 3611e12d-9f59-46b8-8a9d-1564c316d689
timestruct_rms_multithreading = @benchmark rms_multithreading($vals)

# ╔═╡ df265424-2866-4f6a-a001-ec35a3a558a7
rms_multithreading_time = median(timestruct_rms_multithreading.times)

# ╔═╡ 51ab4f90-0691-47d7-b884-0e0532e4cd44
println("Multithreading speed up : $(rms_serial_time/rms_multithreading_time)")

# ╔═╡ 724918cb-4fb2-474f-a012-d3f18663410a
md"

### Χρήσιμες βιβλιοθήκες για multithreading
- [ThreadsX.jl](https://github.com/tkf/ThreadsX.jl)
- [Floops.jl](https://juliafolds.github.io/FLoops.jl/dev/)
- [LoopVectorization.jl](https://juliasimd.github.io/LoopVectorization.jl/stable/)
- [Transducers.jl](https://juliafolds.github.io/Transducers.jl/dev/)
- [Dagger.jl](https://juliaparallel.org/Dagger.jl/dev/)

"

# ╔═╡ Cell order:
# ╠═94d4817e-fe10-11ed-0c87-03b838e33590
# ╠═3b97f8f7-8afb-4632-9208-e3c7e2d4fe19
# ╠═837d0b2f-ab8e-44be-b172-6774ee535f40
# ╠═92cebf2c-eb11-42dd-809c-b151d151e77b
# ╠═fcd842ab-5ee5-4217-910e-e6a8d6e6ab5b
# ╠═8bed2cde-d387-4c43-9daf-2c068af7f4cf
# ╠═3c2e58db-5b3e-46c0-a19a-829104692c33
# ╠═8fe084bb-da68-45a6-965d-a2b1fdfb214e
# ╠═05e42c42-536a-443d-bd67-e8bc85d8ec46
# ╠═88cf22c3-6271-4984-9c6e-d0eeebbf6cbe
# ╠═7a4a13ec-9d3a-41b5-8aa4-4f04172acc30
# ╠═cbbd21ed-95ca-400c-8525-1c060856ac70
# ╠═c4c3a5f2-66ae-4159-8808-51ff502e5467
# ╠═36a310f3-5d16-4874-adf6-3c484ca4dad5
# ╠═a69f5a2c-e7c4-403e-9c0c-2b6a33d56d9d
# ╠═76f7f4c4-8dd5-4411-bd07-751544fc0b97
# ╠═6f46494e-5546-4db2-98cf-9b9186a965da
# ╠═83a3f281-2289-46a8-8409-ae5c3e28227d
# ╠═5955a2b4-6369-4a77-9feb-7138924ee5e5
# ╠═9bd7ffbc-2167-4f6f-9683-56434e696d3b
# ╠═1c0fa66c-f734-4213-bce5-a67e6af397be
# ╟─ac2fe33e-3432-4c46-a41e-fdaf86f22eb6
# ╠═39083364-d3fd-45b4-90c3-11567143239a
# ╠═1ee486c7-b6f2-4e9e-8b7b-cefbb9e5d0fc
# ╠═f3dc2dc6-8ff9-4611-a85a-c972bc465290
# ╠═f175f9fd-5fb2-4f30-91a2-6e1c89852c3d
# ╠═a77d9868-7a61-466d-a0aa-12c219fc4707
# ╟─adac024f-2124-427a-b59f-024097ce156e
# ╠═aa969665-4afe-47ae-b9eb-53be8050b7ef
# ╠═89428484-bc90-4ea3-9c51-e5155bf75e86
# ╠═23ba3460-e68c-4bd4-aa80-bfa4b7148d0c
# ╠═e26febfa-d4b9-4304-bd4b-944d41615f77
# ╠═f6a3068e-90c8-45bd-bcbb-499c604e6a75
# ╠═376f6b50-bd6f-4414-b754-f706374deb4d
# ╟─583e9e4b-aebe-4359-a676-3cecce1371d5
# ╠═1797cbaf-54de-4aa4-9a23-bfc4491b5ea9
# ╠═ce915cf7-0dd4-4c12-af70-c1b2be6718d0
# ╠═43307b99-63a0-41c3-b893-6ffc3c1a09df
# ╠═431d6ef7-10c9-4dac-8f8a-ecb253847276
# ╠═d793b58d-e3fa-4253-b901-4eaef0548ffb
# ╠═33307e72-8fa3-4a1c-b5ea-7093ce98b7b1
# ╠═ea18a8fe-6c99-4338-ab25-a34c7e4a7bd7
# ╠═bc33caf2-5846-4db0-8533-ba3144a25d7d
# ╠═a5f030ab-b63d-47cb-980f-2fd66f3e8e83
# ╠═7e1a0d09-69c9-4225-9962-5a8031d92ba4
# ╠═5090a103-100e-4ac3-a465-680da04c4ebb
# ╠═817b2063-971c-4a2d-8748-2bd6f75e4e90
# ╠═f9a7b44d-716a-45d7-b4c3-361fb271cea2
# ╠═911bf409-519e-4dc7-9572-22119b2a5e6f
# ╠═aa8c6863-8d63-4bf0-88da-7f1116173502
# ╠═43853644-248d-4307-a8fc-82ffb812a990
# ╠═4a274bab-ede4-4191-bb4c-bbb66f37bb31
# ╠═d208d8ca-f8c2-4c1b-b967-3f300c098aa0
# ╠═0ec3889d-d204-494f-b8dc-9fa8c7dbe6f5
# ╟─d7669c3f-0194-4932-b760-aee5b69eed8c
# ╠═8b111fb6-494e-447c-bbcd-4a282ee1da3a
# ╠═8d5cdebc-9d06-4b9c-8806-db2f41d3ee3d
# ╠═7c4c2d07-651b-48dd-bb24-1ac65b045558
# ╟─25f93517-0714-4a08-a78c-9deb6aab92eb
# ╠═a86f69e5-2bde-4c59-96e7-e4f42e62a3ee
# ╠═2991d7cc-5fe3-4dc1-8e1c-85420f9ed82f
# ╠═cf153a5c-2805-417a-b460-19325ad4b37a
# ╠═077791d1-710d-4f23-a7af-a6d2327d1b99
# ╟─8a21233a-5ef1-4b01-91c4-9640aedeed2a
# ╠═a6f2575c-ea7b-4aa4-8afd-12ed9f7c986b
# ╠═f09486c4-f575-471f-8e35-5e8c5d8e9646
# ╠═886624b3-264e-4423-a587-64e59433dc0a
# ╠═3611e12d-9f59-46b8-8a9d-1564c316d689
# ╠═df265424-2866-4f6a-a001-ec35a3a558a7
# ╠═51ab4f90-0691-47d7-b884-0e0532e4cd44
# ╟─724918cb-4fb2-474f-a012-d3f18663410a
