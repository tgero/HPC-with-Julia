%%
clc
N = 10^7;
a = rand(1,N);

%%
%  Χρόνος εκτέλεσης του αθροίσματος N=10^8 αριθμών με χρήση for loop
%
%

t0 = clock ();
my_sum(a);
elapsed_time = etime (clock (), t0);
disp(["my_sum (N=10^7): " num2str(elapsed_time*10^3) " ms"])


%%
%  Χρόνος εκτέλεσης του αθροίσματος N=10^8 αριθμών με χρήση του
%  build-in sum
%
%

N = 10^8;
a = rand(1,N);
t0 = clock ();
sum(a);
elapsed_time = etime (clock (), t0);
disp(["sum (N=10^8): " num2str(elapsed_time*10^3) " ms"])

%%
%
%  Aθροίσμα N=10^8 αριθμών με χρήση for loop
%
#function  S = my_sum(x)
#   S = 0.;
#   for i=1:length(x)
#      S = S + x(i);
#   end
#end


