#include <iostream>
#include <chrono>
#include <Eigen/Dense>

using Eigen::VectorXd;

using namespace std::chrono;

int main() {

    int n =100000000;

    VectorXd v = VectorXd::Random(n);
  
    auto start = high_resolution_clock::now();

    double result = v.sum();

    auto stop = high_resolution_clock::now();
    
    duration<double, std::milli> elapsed_time = stop - start;

    std::cout << "Results: " << result << std::endl;

    std::cout << "Time taken by function my_eigen_sum: "
         << elapsed_time.count() << " milliseconds" << std::endl;

    return 0;
}
