%%
%
%  Aθροίσμα N=10^8 αριθμών με χρήση for loop
%

function  S = my_sum(x)
   S = 0.;
   for i=1:length(x)
      S = S + x(i);
   end
end
