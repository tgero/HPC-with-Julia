%%
1;
clc
N = 10^8;
a = rand(1,N);


%%
tic();
my_sum(a);
toc();


%%
%  Χρόνος εκτέλεσης του αθροίσματος N=10^8 αριθμών με χρήση for loop
%
%

f_my_sum = @() my_sum(a);
elapsed_time = timeit(f_my_sum);
disp(num2str(elapsed_time*10^3)+" ms")

%%
%  Χρόνος εκτέλεσης του αθροίσματος N=10^8 αριθμών με χρήση του
%  build-in sum
%
%

f = @() sum(a);
elapsed_time = timeit(f);
disp(num2str(elapsed_time*10^3)+" ms")

%%
%
%  Aθροίσμα N=10^8 αριθμών με χρήση for loop
%
%function  S = my_sum(x)
%   S = 0.;
%   for i=1:length(x)
%      S = S + x(i);
%   end
%end


