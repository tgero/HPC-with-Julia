%%
clc
N = 10^8;
a = rand(1,N);
b = rand(1,N);

%%
%  Χρόνος εκτέλεσης του εσωτερικού γινομένου διανυσμάτων διάστασης N=10^8 
%  με for loop
%

f_my_dot_product =  @() my_dot_product(a,b);
elapsed_time = timeit(f_my_dot_product);
disp(num2str(elapsed_time*10^3)+" ms")

%%
%  Χρόνος εκτέλεσης του εσωτερικού γινομένου διανυσμάτων διάστασης N=10^8 
%  με πράξεις πινάκων
%
%
f = @() a*b';

elapsed_time = timeit(f);
disp(num2str(elapsed_time*10^3)+" ms")

%%

f_dot = @() dot(a, b);
elapsed_time = timeit(f_dot);
disp(num2str(elapsed_time*10^3)+" ms")

%%
%
%  Aθροίσμα N=10^8 αριθμών με χρήση for loop
%
function S = my_dot_product(x,y)
   S = 0.;
   for i=1:length(x)     
      S = S + x(i)*y(i);
   end
end


