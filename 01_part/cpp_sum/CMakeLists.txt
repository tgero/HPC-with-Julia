cmake_minimum_required(VERSION 3.0.0)
project(cpp_sum VERSION 0.1.0)

include(CTest)
enable_testing()

if (MSVC)
   SET(CMAKE_CXX_FLAGS "/Ox /fp:fast /arch:AVX2 /openmp")
else()
   SET(CMAKE_CXX_FLAGS "-O3 -march=znver3 -ffast-math -fopenmp  -mavx2")
endif()

add_executable(cpp_sum main.cpp)

set_property(TARGET cpp_sum PROPERTY CXX_STANDARD 20)
set_property(TARGET cpp_sum PROPERTY CXX_STANDARD_REQUIRED On)
set_property(TARGET cpp_sum PROPERTY CXX_EXTENSIONS Off)



set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
