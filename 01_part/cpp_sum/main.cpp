#include <iostream>
#include <cstdlib>

#include <chrono>

#include <vector>
#include <numeric>
#include <omp.h>

using namespace std;
using namespace std::chrono;

double my_sum(double* x, const int& n)
{
   double S = 0.;

   for(int i = 0; i<n; i++ ){
    S += x[i];
   }

   return S;
}


double my_sum_vector(const vector<double>& x)
{
   double S = 0.;

 //  cout << " x.size() = " << x.size() << endl;

   for(int i = 0; i<x.size(); i++ ){
    S += x[i];
   }
   
   return S;
}


//set OMP_NUM_THREADS=4
double my_omp_sum(double* x, const int& n)
{
   double S = 0.;
  
   #pragma omp parallel for reduction (+:S)
   for(int i = 0; i<n; i++ ){
    S += x[i];
   }

   return S;
}


int main() {

    int n=100000000;
   
    cout << "n = " << n << endl;

    double *a = new double[n];

    srand((unsigned) time(NULL));

    for(int i=0;i<n; i++)
        a[i] = (double) rand()/RAND_MAX;
      
    auto start = high_resolution_clock::now();
 
    double result = my_sum(a, n);

    auto stop = high_resolution_clock::now();
    
    duration<double, std::milli> elapsed_time = stop - start;

    cout << "Results: " << result << endl;

    cout << "Time taken by function my_sum: "
         << elapsed_time.count() << " milliseconds" << endl;

    // -------------------------

    vector<double> ad(n);
    for(int i=0;i<n; i++)
        ad[i] = (double) rand()/RAND_MAX;

    start = high_resolution_clock::now();
    result = my_sum_vector(ad);
    stop = high_resolution_clock::now();
    elapsed_time = stop - start;
    
    cout << "Results: " << result << endl;


    cout << "Time taken by function my_sum_vector: "
         << elapsed_time.count() << " milliseconds" << endl;

    // -------------------------

    start = high_resolution_clock::now();
    result = accumulate(ad.begin(), ad.end(), 0.0);
    stop = high_resolution_clock::now();
    elapsed_time = stop - start;
    
    cout << "Results: " << result << endl;


    cout << "Time taken by function accumulate: "
         << elapsed_time.count() << " milliseconds" << endl;

    // -------------------------
    
    start = high_resolution_clock::now();
    double omp_result;
    omp_result = my_omp_sum(a, n);
    stop = high_resolution_clock::now();
    elapsed_time = stop - start;
    
    cout << "Results: " << omp_result << endl;

               cout << "Time taken by function my_omp_sum: "
         << elapsed_time.count() << " milliseconds" << endl;


    delete [] a;

    return 0;
}
