#=

Παράδειγμα χρήσης της Julia για την επίλυση 
μιας απλής ODE με κεντρικές διαφορές

Θεόδωρος Γεροστάθης
27/03/2023

=#


using LinearAlgebra
using PyPlot


"""
    simple_FD_solver(N, α, β, f )

    Υπολογίζει την αριθμητική λύση της uₓₓ = f(x) στο [0, 1] για u(0) = α, u(1) = β με κεντρικές διαφορές
    χρησιμοποιώντας μια ομοιόμορφή διαμέριση του [0, 1] σε N διαστήματα.

    # Παράδειγμα:
    ```julia-repl
    julia> α = 1
    julia> β = 1
    julia> xi, u_numerical = simple_FD_solver(401, α, β, exp )
    ```
"""
function simple_FD_solver(N, α, β, f )

    h = 1/(N+1)

    xi = collect(LinRange(0,1,N+2));

    xi_internal = xi[begin+1:end-1]

    A = diagm(-1 => ones(N-1), 0 => ones(N)*(-2), 1=>ones(N-1))/h^2

    b = [f(xi_internal[begin])-α/h^2; f.(xi_internal[begin+1:end-1]);  f(xi_internal[end])-β/h^2]

    ui = A\b

    u_numerical = [α; ui; β];

    xi, u_numerical

end


function main()

    α = 1
    β = 1
    xi, u_numerical = simple_FD_solver(401, α, β, exp )

    a = - exp(0) + α;
    b = β - a - exp(1);

    u_exact(x) = exp(x)+ b*x + a   # Exact solution of uₓₓ = eˣ

    plot(xi, u_numerical, "ok",label="Numerical", alpha=.1, ms=8)
    plot(xi, u_exact.(xi),"-r", label="Exact")
    grid(true)
    xlabel("x")
    ylabel("u(x)")
    legend()
    display(gcf())

    nothing

end


main()
